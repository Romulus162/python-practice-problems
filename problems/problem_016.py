# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.


def isInsideBounds(x, y):
    count = 0
    for i in [x, y]:
        if i >= 0 and i <= 10:
            count += 1
    if count == 2:
        print("test passed")
        return True
    else:
        print("doesn't pass test")


isInsideBounds(3, 8)
isInsideBounds(15, 4)
