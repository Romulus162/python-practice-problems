# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# import problem_010
# import problem_011


# def fizzbuzz(number):
#     problem_010.is_divisible_by_3(number) and problem_011.is_divisible_by_5(number)
#     problem_010.is_divisible_by_3(number)
#     problem_011.is_divisible_by_5(number)

### I wanted to try doing this via the import way, but its taking too long, although it would be cooler if I completed it this way


def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0:
        print("Fizzbuzz")
    elif number % 3 == 0:
        print("fizz")
    elif number % 5 == 0:
        print("buzz")
    else:
        return f"{number} isn't divisible by 3 or 5"


# fizzbuzz(3)
# fizzbuzz(5)
# fizzbuzz(15)
# fizzbuzz(2)
