# Write a class that meets these requirements.
#
# Name:       Student
#
# Required state:
#    * name, a string
#
# Behavior:
#    * add_score(score)   # Adds a score to their list of scores
#    * get_average()      # Gets the average of the student's scores
#
# Example:
#    student = Student("Malik")
#
#    print(student.get_average())    # Prints None
#    student.add_score(80)
#    print(student.get_average())    # Prints 80
#    student.add_score(90)
#    student.add_score(82)
#    print(student.get_average())    # Prints 84
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class Student:
    i = 0
    counter = 0

    def __init__(self, name):
        self.name = name

    def addScore(self, score):
        self.i += score
        self.counter += 1

    def getAverage(self):
        if self.counter == 0:
            return None
        return self.i / self.counter


student = Student("Malik")

#
# print(student.getAverage())  # Prints None
# student.addScore(80)
# print(student.getAverage())  # Prints 80
# student.addScore(90)
# student.addScore(82)
# print(student.getAverage())  # Prints 84
