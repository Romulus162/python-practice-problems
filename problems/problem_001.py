# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def minimumvalue(value1, value2):
    if value1 == value2:
        return print(f"Both values({value1}, {value1}) are the same")
    if value1 < value2:
        return value1
    if value2 < value1:
        return value2
