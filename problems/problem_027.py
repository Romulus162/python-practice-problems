# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

import problem_024


def maxInList(values):
    if problem_024.checkIfZero(values):
        return None
    else:
        return print(max(values))


# max_in_list([0, 2, 3, 8, 9999999999])
