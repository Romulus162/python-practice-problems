# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def canMakePasta(ingredients):
    confirm = ["flour", "eggs", "oil"]
    found = 0
    for i in ingredients:
        for j in confirm:
            if i == j:
                found += 1
                break
        if found == len(confirm):
            print("You have all required ingredients")
            return True
    print("sorry, you don't have the right ingredients")
    return False


test1 = ["flour", "eggs", "oil"]
test2 = [1, 2, 3]

canMakePasta(test1)
canMakePasta(test2)
