# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them ----
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def maxOfThree(value1, value2, value3):
    maximum = max(value1, value2, value3)

    if value1 == value2 == value3:
        return print(f"Every value you gave is identical ({value1})")

    test1 = value1 == value2
    test2 = value1 == value3
    test3 = value3 == value2

    if test1 or test2 or test3:
        if test1 == maximum:
            return print(f"Values {value1} and {value2} are both the biggest numbers")
        elif test2 == maximum:
            return print(f"Values {value1} and {value3} are both the biggest numbers")
        else:
            return print(f"Values {value3} and {value2} are both the biggest numbers")
