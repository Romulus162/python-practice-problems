# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem


def shiftLetters(word):
    list = []
    list2 = []
    for i in word:
        if i == "z":
            num = 97
        else:
            num = ord(i) + 1
            if num == 98 and i == "z":
                num = 97
        list.append(num)
    for o in list:
        num1 = chr(o)
        list2.append(num1)
    return "".join(list2)


# print(shiftLetters("import"))
# print(shiftLetters("ABBA"))
# print(shiftLetters("Kala"))
# print(shiftLetters("zap"))
