# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def sumOfFirstNEvenNumbers(n):
    if n < 0:
        return None
    x = 0
    i = 0
    while x < n:
        i += (x + 1) * 2
        x += 1

    return i


# print(sumOfFirstNEvenNumbers(-1))
# print(sumOfFirstNEvenNumbers(0))
# print(sumOfFirstNEvenNumbers(5))
# print(sumOfFirstNEvenNumbers(1))
# print(sumOfFirstNEvenNumbers(2))
# print(sumOfFirstNEvenNumbers(3))
