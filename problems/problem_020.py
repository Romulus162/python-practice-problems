# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


def hasQuorum(attendeesList, membersList):
    half = membersList / 2
    if attendeesList >= half:
        print("half or more members are attending")
        return True
    else:
        print("less than half of the members showed up today")
        return False
