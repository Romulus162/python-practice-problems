# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]


def halveTheList(list):
    result = []
    for i in range(0, len(list), 2):
        result.append(list[i : i + 2])
    return result


# print(halveTheList([1, 2, 3, 4, 5, 6]))
# print(halveTheList([1, 2, 3, 4]))
# print(halveTheList([1, 2, 3]))
