# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

# NOTICE
# this code commented out below also works, however I already solved it this
# way on a dictionary problem 4 task 2 from the first week of the bootcamp.
# You can check it out in the #solutions chat in slack

# with this in mind I wanted to challenge myself and find a shorter way to
# write this function if possible


# WORKING CODE

# def reverse_dictionary(dictionary):
#     keys = []
#     values = []
#     for i, o in dictionary.items():

#         keys.append(i)
#         values.append(o)

#     Zipped = dict(zip(values, keys))
#     return Zipped


# WORKING CODE


# shortened code
def reverseDictionary(dictionary):
    return {v: k for k, v in dictionary.items()}


# this is very awesome because it didn't even occur to me that I could
# write code inside a dictionary itself like this with a for loop and
# everything
# that being said,
# while reading the comments in stack overflow to find this answer
# someone mentioned this lacks or wouldn't have "unicity"
# after some research indeed that is true as it turns out that
# in dictionaries keys must be unique, and if they are the same somewhere
# the data essentially gets lost,
# I think that's beyond this exercise as of now, but for future reference
# it would be a good idea for me to include or push "same keys"
# into the same value area of the new reversed key/value set dictionary

# print(reverseDictionary({}))
# print(reverseDictionary({"key": "value"}))
# print(reverseDictionary({"one": 1, "two": 2, "three": 1}))
