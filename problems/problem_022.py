# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"


def gearForDay(isWorkday, isSunny):
    gear = []
    if not isSunny and isWorkday:
        print("not sunny and workday, get umbrella")
        gear.append("umbrella")
    if isWorkday:
        print("its a workday")
        gear.append("Laptop")
    if not isWorkday:
        print("Weekend, time to surf!!!")
        gear.append("surfboard")

    return print(", ".join(gear))


# gearForDay(True, False)
# gearForDay(False, True)
# gearForDay(False, False)
# gearForDay(True, True)
