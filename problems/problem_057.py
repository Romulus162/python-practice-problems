# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

from fractions import Fraction


def sumFractionSequence(num):
    list = []
    # newList = []
    for i in range(1, num + 1):
        list.append(Fraction(i, i + 1))
    # for s in list:
    # newList.append(str(s))
    twoBirdsOneStone = [str(i) for i in list]
    return " + ".join(twoBirdsOneStone)


# print(sumFractionSequence(3))
