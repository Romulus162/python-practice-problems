# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

import problem_024


def removeDuplicateLetters(s):
    if problem_024.checkIfZero(s):
        return s
    else:
        return print("".join(sorted(set(s))))


# remove_duplicate_letters(";alskdfjl;asdjfo;aj")
