# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def canSkydive(age, hasConsentForm):
    if age >= 18 or hasConsentForm:
        print("You may go skydiving")
    else:
        print("unfortunatly you are too young to jump out of a plane")


# person >= 18 or consentForm
