# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you


def checkIfZero(z):
    length = len(z)
    if length == 0:
        return True
    else:
        return False


def calculateAverage(values):
    if checkIfZero(values):
        return None
    else:
        x = 0
        for i in values:
            x += i
    average = x / len(values)
    return average
