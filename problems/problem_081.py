# Write four classes that meet these requirements.
#
# Name:       Animal
#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Animal:
    def __init__(self, numLegs, primeColor):
        self.numLegs = numLegs
        self.primeColor = primeColor

    def describe(self):
        return (
            self.__class__.__name__
            + " has "
            + str(self.numLegs)
            + " legs and is primarily "
            + self.primeColor
        )


class Dog(Animal):
    def __init__(self, numLegs, primeColor):
        super().__init__(numLegs, primeColor)

    def speak(self):
        return "Bark"


class Cat(Animal):
    def __init__(self, numLegs, primeColor):
        super().__init__(numLegs, primeColor)

    def speak(self):
        return "Miao"


class Snake(Animal):
    def __init__(self, numLegs, primeColor):
        super().__init__(numLegs, primeColor)

    def speak(self):
        return "Sssssss"


# dog = Dog(4, "Black")
# print(dog.describe())
# print(dog.speak())

# dog = Cat(4, "White")
# print(dog.describe())
# print(dog.speak())

# dog = Snake(0, "Green")
# print(dog.describe())
# print(dog.speak())
