# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

import problem_024


def calculateGrade(values):
    if any(
        v > 100 or v < 0 for v in values
    ):  # this is something new I learned while trying to find a simple answer, I did not know I could call the "for/in" loop at the end of its scope like so
        print("Error, number doesn't fit in grading system")
        return False
    else:
        grade = problem_024.calculate_average(values)
        if grade >= 90:
            return "You got an A!!!"
        elif grade >= 80:
            return "You got a B"
        elif grade >= 70:
            return "You got a C"
        elif grade >= 60:
            return "You got a D"
        else:
            return "You Failed"
