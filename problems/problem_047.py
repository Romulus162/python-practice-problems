# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"


def lowerCheck(list):
    for item in list:
        if item.islower():
            return True


def upperCheck(list):
    for item in list:
        if item.isupper():
            return True


def digitCheck(list):
    for item in list:
        if item.isdigit():
            return True


def charCheck(list):
    for item in list:
        if item in ["$", "!", "@"]:
            return True


def checkPassword(password):
    length = len(password)
    big = True if length <= 12 else False
    small = True if length >= 6 else False
    lower = True if lowerCheck(password) else False
    upper = True if upperCheck(password) else False
    digit = True if digitCheck(password) else False
    char = True if charCheck(password) else False

    if big and small and lower and upper and digit and char:
        return "Strong Password aquired"
    elif big is False:
        return "Your password is too long"
    elif small is False:
        return "Your password is too small"
    elif digit is False:
        return "Your password doesn't have any numbers"
    elif char is False:
        return "Your password doesn't have any special characters ($, !, or @)"
    elif lower is False:
        return "Your password needs a lower-case letter"
    elif upper is False:
        return "Your password needs a upper-case letter"


# print(checkPassword("Fa13!352"))

# too long
# too small
# needs uppercase
# needs lowercase
# doesn't have any special characers
# strong password aquired
