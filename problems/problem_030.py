# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

import problem_024


def findSecondLargest(values):
    if problem_024.checkIfZero(values):
        return None
    if len(values) == 1:
        return None
    else:
        sortedNums = sorted(values)
        secondLargest = sortedNums[-2]
        return print(secondLargest)


test = [1, 8, 235, 3, 5, 98243]
findSecondLargest(test)
