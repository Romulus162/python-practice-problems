# Write a class that meets these requirements.
#
# Name:       BankAccount
#
# Required state:
#    * opening balance, the amount of money in the bank account
#
# Behavior:
#    * get_balance()      # Returns how much is in the bank account
#    * deposit(amount)    # Adds money to the current balance
#    * withdraw(amount)   # Reduces the current balance by amount
#
# Example:
#    account = BankAccount(100)
#
#    print(account.get_balance())  # prints 100
#    account.withdraw(50)
#    print(account.get_balance())  # prints 50
#    account.deposit(120)
#    print(account.get_balance())  # prints 170
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.


class BankAccount:
    def __init__(self, current):
        self.current = current

    def getBalance(self):
        return self.current

    def deposit(self, amount):
        self.current += amount

    def withdraw(self, amount):
        self.current -= amount


account = BankAccount(100)

# print(account.getBalance())  # prints 100
# account.withdraw(50)
# print(account.getBalance())  # prints 50
# account.deposit(120)
# print(account.getBalance())  # prints 170
