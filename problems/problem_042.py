# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.


def pairwiseAdd(list1, list2):
    answer = []
    for x, y in zip(list1, list2):
        sum = x + y
        answer.append(sum)
    return answer


print(pairwiseAdd([1, 2, 3, 4], [4, 5, 6, 7]))


# visual aid for why error occured and I had to had zip() method

# something = for x, y in [1, 2, 3, 4],  [4, 5, 6, 7]:

# zip turns this into

# something1 = for x, y in [[1,4], [2, 5], [3, 6], [4, 7]]

# now x and y are able to isolate the values
