# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

import problem_024


def calculateSum(values):
    if problem_024.checkIfZero(values):
        return None
    else:
        x = 0
        for i in values:
            x += i
    return print(x)


# calculate_sum([2, 4, 6, 8])  # 20
